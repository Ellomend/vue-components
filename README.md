# vue-components

> Vue components tutorial

## Pages
> Pages correlate with [Vue documentation](https://vuejs.org/v2/guide/components.html)

* Using components
  * Registration
  * Local registration
  * data Must Be a Function
  * Composing Components
* Props  
  * Passing Data with Props
  * camelCase vs. kebab-case
  * Dynamic Props
  * Literal vs. Dynamic
  * One-Way Data Flow
  * Prop Validation
* Non-Prop Attributes
  * Replacing/Merging with Existing Attributes

* Custom Events
  * Using v-on with Custom Events
  * .sync Modifier
  * Form Input Components using Custom Events
  * Customizing Component v-model
  * Non Parent-Child Communication
* Content Distribution with Slots
  * Compilation Scope
  * Single Slot
  * Named Slots
  * Scoped Slots
* Dynamic Components
  * keep-alive
* Misc
  * Authoring Reusable Components
  * Child Component Refs
  * Async Components
  * Advanced Async Components
  * Component Naming Conventions
  * Recursive Components
  * Circular References Between Components
  * Inline Templates
  * X-Templates
  * Cheap Static Components with v-once
