import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/pages/home'
import UsingComponents from '@/components/pages/UsingComponents/index'
import Registration from '@/components/pages/UsingComponents/registration'

// import pages
import localRegistration from '../components/pages/UsingComponents/localRegistration.vue'
import dataFunction from '../components/pages/UsingComponents/dataFunction.vue'
import composingComponents from '../components/pages/UsingComponents/composingComponents.vue'

// props
import Props from '../components/pages/Props/index.vue'
import passingData from '../components/pages/Props/passingData.vue'
import camelKebab from '../components/pages/Props/camelKebab.vue'
import dynamicProps from '../components/pages/Props/dynamicProps.vue'
import literalDynamic from '../components/pages/Props/literalDynamic.vue'
import oneWayDataFlow from '../components/pages/Props/oneWayDataFlow.vue'
import propValidation from '../components/pages/Props/propValidation.vue'

// Custom events
import CustomEvents from '../components/pages/CustomEvents/index.vue'
import vOnCustomEvents from '../components/pages/CustomEvents/vOnCustomEvents.vue'
import syncModifier from '../components/pages/CustomEvents/syncModifier.vue'
import formInputComponentsUsingCustomEvents from '../components/pages/CustomEvents/formInputComponentsUsingCustomEvents.vue'
import customizingComponentVModel from '../components/pages/CustomEvents/customizingComponentVModel.vue'
import nonParentChildCommunication from '../components/pages/CustomEvents/nonParentChildCommunication.vue'

// Slots
import Slots from '../components/pages/Slots/index.vue'
import compilationScope from '../components/pages/Slots/compilationScope.vue'
import singleSlot from '../components/pages/Slots/singleSlot.vue'
import namedSlots from '../components/pages/Slots/namedSlots.vue'
import scopedSlots from '../components/pages/Slots/scopedSlots.vue'

// DynamicComponents
import DynamicComponents from '../components/pages/DynamicComponents/index.vue'
import keepAlive from '../components/pages/DynamicComponents/keepAlive.vue'

// Non props attributes
import NonPropAttributes from '../components/pages/NonPropAttributes/index.vue'
import replacingMergingAttributes from '../components/pages/NonPropAttributes/replacingMergingAttributes.vue'

// Misc
import Misc from '../components/pages/Misc/index.vue'
import authoringReusableComponents from '../components/pages/Misc/authoringReusableComponents.vue'
import childComponentRefs from '../components/pages/Misc/childComponentRefs.vue'
import asyncComponents from '../components/pages/Misc/asyncComponents.vue'
import advancedAsyncComponents from '../components/pages/Misc/advancedAsyncComponents.vue'
import componentNamingConventions from '../components/pages/Misc/componentNamingConventions.vue'
import recursiveComponents from '../components/pages/Misc/recursiveComponents.vue'
import circularReferencesBetweenComponents from '../components/pages/Misc/circularReferencesBetweenComponents.vue'
import inlineTemplates from '../components/pages/Misc/inlineTemplates.vue'
import xTemplates from '../components/pages/Misc/xTemplates.vue'
import cheapStaticComponentsWithVOnce from '../components/pages/Misc/cheapStaticComponentsWithVOnce.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/using-components',
      name: 'UsingComponents',
      component: UsingComponents,
      children: [
        {
          path: 'registration',
          name: 'registration',
          component: Registration
        },
        {
          path: 'local-registration',
          name: 'localRegistration',
          component: localRegistration
        },
        {
          path: 'data-function',
          name: 'dataFunction',
          component: dataFunction
        },
        {
          path: 'composing-components',
          name: 'composingComponents',
          component: composingComponents
        }
      ]
    },
    {
      path: '/props',
      name: 'props',
      component: Props,
      children: [
        {
          path: 'passing-data',
          name: 'passingData',
          component: passingData
        },
        {
          path: 'camel-kebab',
          name: 'camelKebab',
          component: camelKebab
        },
        {
          path: 'dynamic-props',
          name: 'dynamicProps',
          component: dynamicProps
        },
        {
          path: 'literal-dynamic',
          name: 'literalDynamic',
          component: literalDynamic
        },
        {
          path: 'one-way-data-flow',
          name: 'oneWayDataFlow',
          component: oneWayDataFlow
        },
        {
          path: 'prop-validation',
          name: 'propValidation',
          component: propValidation
        }
      ]
    },
    {
      path: '/non-prop-attributes',
      name: 'nonPropAttributes',
      component: NonPropAttributes,
      children: [
        {
          path: 'replacing-mergig-attributes',
          name: 'replacingMergingAttributes',
          component: replacingMergingAttributes
        }
      ]
    },
    {
      path: '/custom-events',
      name: 'customEvents',
      component: CustomEvents,
      children: [
        {
          path: 'using-v-on-with-custom-events',
          name: 'vOnCustomEvents',
          component: vOnCustomEvents
        },
        {
          path: 'sync-modifier',
          name: 'syncModifier',
          component: syncModifier
        },
        {
          path: 'form-input-components-using-custom-events',
          name: 'formInputComponentsUsingCustomEvents',
          component: formInputComponentsUsingCustomEvents
        },
        {
          path: 'customizing-component-v-model',
          name: 'customizingComponentVModel',
          component: customizingComponentVModel
        },
        {
          path: 'non-parent-child-communication',
          name: 'nonParentChildCommunication',
          component: nonParentChildCommunication
        }
      ]
    },
    {
      path: '/content-distribution-with-slots',
      name: 'contentDistributionWithSlots',
      component: Slots,
      children: [
        {
          path: 'compilation-scope',
          name: 'compilationScope',
          component: compilationScope
        },
        {
          path: 'single-slot',
          name: 'singleSlot',
          component: singleSlot
        },
        {
          path: 'named-slots',
          name: 'namedSlots',
          component: namedSlots
        },
        {
          path: 'Scoped Slots',
          name: 'scopedSlots',
          component: scopedSlots
        }
      ]
    },
    {
      path: '/dynamic-components',
      name: 'dynamicComponents',
      component: DynamicComponents,
      children: [
        {
          path: 'keep-alive',
          name: 'keepAlive',
          component: keepAlive
        }
      ]
    },
    {
      path: '/misc',
      name: 'misc',
      component: Misc,
      children: [
        {
          path: 'authoring-reusable-components',
          name: 'authoringReusableComponents',
          component: authoringReusableComponents
        },
        {
          path: 'child-component-refs',
          name: 'childComponentRefs',
          component: childComponentRefs
        },
        {
          path: 'async-components',
          name: 'asyncComponents',
          component: asyncComponents
        },
        {
          path: 'advanced-async-components',
          name: 'advancedAsyncComponents',
          component: advancedAsyncComponents
        },
        {
          path: 'component-naming-conventions',
          name: 'componentNamingConventions',
          component: componentNamingConventions
        },
        {
          path: 'recursive-components',
          name: 'recursiveComponents',
          component: recursiveComponents
        },
        {
          path: 'circular-references-between-components',
          name: 'circularReferencesBetweenComponents',
          component: circularReferencesBetweenComponents
        },
        {
          path: 'inline-templates',
          name: 'inlineTemplates',
          component: inlineTemplates
        },
        {
          path: 'x-templates',
          name: 'xTemplates',
          component: xTemplates
        },
        {
          path: 'cheap-static-components-with-v-once',
          name: 'cheapStaticComponentsWithVOnce',
          component: cheapStaticComponentsWithVOnce
        }
      ]
    }
  ]
})
