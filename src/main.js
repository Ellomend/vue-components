// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import MuseUI from 'muse-ui'
import 'muse-ui/dist/muse-ui.css'
import 'muse-ui/dist/theme-light.css' // use carbon theme
import VueCodeMirror from 'vue-codemirror'
// import global components
import DefaultLayout from './components/layouts/default.vue'
Vue.config.productionTip = false

Vue.use(MuseUI)
Vue.use(VueCodeMirror)
// register global components
Vue.component('DefaultLayout', DefaultLayout)
/* eslint-disable no-new */

/*
* FUN STUFF
* data Function
* */

var data = { counter: 0 }
Vue.component('simple-counter', {
  template: '<button v-on:click="counter += 1">{{ counter }}</button>',
  // data is technically a function, so Vue won't
  // complain, but we return the same object
  // reference for each component instance
  data: function () {
    return data
  }
})
Vue.component('simple-counter2', {
  template: '<button v-on:click="counter += 1">{{ counter }}</button>',
  // data is technically a function, so Vue won't
  // complain, but we return the same object
  // reference for each component instance
  data: function () {
    return {
      counter: 0
    }
  }
})
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})
